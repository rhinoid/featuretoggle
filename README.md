A small helper to have FeatureToggles in your iOS/OSX project

A feature Toggle is a way to allow multiple teams which work in a single codebase to merge often instead of waiting until a feature is ready and than potentially facing 'merge hell'. The trick is that by placing your feature behind a featuretoggle, you can just set this to the off position when merging to develop. This way the other teams can conteinue pulling in develop in their feature branches without having half working software on their branch. It also allows develop to be released at any moment.

In essence a feature toggle is not more than a compile time constant or an #ifdef. This helper has a few more features though:
 - It allows arbitrary nesting/grouping of features 
 - Whole groups can be turned on / off
 - It's a runtime check, so it allows a server component to decide whats on or off.
 - It gives asserts when wrong things are checked
 - It has proper IDE auto completion.

To see how to use it check out the extremely simple test project, or better yet run the tests.

The solution used to create this helper seems a bit over engineered, but in reality this solution helped making the nesting of featuretoggles a simple affair, instead of having to declare whole class hierarchies which is a lot of work an quite error prone.