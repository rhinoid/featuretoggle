//
//  AppDelegate.h
//  FeatureToggle
//
//  Created by Reinier van Vliet on 30/01/15.
//  Copyright (c) 2015 POC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

