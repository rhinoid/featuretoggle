//
//  main.m
//  FeatureToggle
//
//  Created by Reinier van Vliet on 28/09/15.
//  Copyright (c) 2015 POC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
