//
//  ViewController.m
//  FeatureToggle
//
//  Created by Reinier van Vliet on 30/01/15.
//  Copyright (c) 2015 POC. All rights reserved.
//

#import "ViewController.h"
#import "toggle.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    
    if (app.section1.subsection1.feature1.isEnabled) {
        NSLog(@"feature1 is enabled");
    } else {
        NSLog(@"feature1 is disabled");
    }
    
    if (app.section1.subsection1.feature2.isEnabled) {
        NSLog(@"feature2 is enabled");
    } else {
        NSLog(@"feature2 is disabled");
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
