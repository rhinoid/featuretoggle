//
//  Toggle.h
//  FeatureToggle
//
//  Created by Reinier van Vliet on 29/01/15.
//  Copyright (c) 2015 POC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Toggle : NSObject<NSCopying>
- (instancetype)initWithName:(NSString *)name;
- (BOOL)isEnabled;
- (BOOL)isEqual:(id)object;
- (NSUInteger)hash;
- (id)copyWithZone:(NSZone *)zone;

@property (nonatomic, strong, readonly) NSString *name;

//help with intellisense
- (Toggle *)app;
- (Toggle *)section1;
- (Toggle *)subsection1;
- (Toggle *)feature1;
- (Toggle *)feature2;

@end

extern Toggle *app;

