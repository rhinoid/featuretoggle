//
//  Toggle.m
//  FeatureToggle
//
//  Created by Reinier van Vliet on 29/01/15.
//  Copyright (c) 2015 POC. All rights reserved.
//

#import "Toggle.h"
#import "FeatureList.h"

@interface Toggle()

- (instancetype)initWithName:(NSString *)name;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) Toggle *lastChildToggle;
@end

Toggle *app;

__attribute__((constructor))
static void initialize_global_app_toggle() {
    @autoreleasepool {
        app = [[Toggle alloc] initWithName:@"app"];
    }
}

#pragma clang diagnostic ignored "-Wincomplete-implementation"
@implementation Toggle

- (instancetype)initWithName:(NSString *)name
{
    self = [super init];
    if (self) {
        _name = name;
    }
    return self;
}

-(BOOL)isEnabled{
    return [FeatureList.sharedInstance isEnabled:self];
}

-(Toggle *)getToggleForProperty:(NSString *)name{
    Toggle *childToggle = [[Toggle alloc] initWithName:[[self.name stringByAppendingString:@"."] stringByAppendingString:name]];
    return childToggle;
}

- (NSMethodSignature *)methodSignatureForSelector:(SEL)sel {
    id	stringSel = NSStringFromSelector(sel);
    unsigned long	numParams = [[stringSel componentsSeparatedByString:@":"] count] - 1u;
    if (numParams == 0){
        return [super methodSignatureForSelector:@selector(getToggleForProperty:)];
    }
    return nil;
}
    
- (void)forwardInvocation:(NSInvocation *)invocation {
    id	stringSel = NSStringFromSelector([invocation selector]);
    unsigned long numParams = [[stringSel componentsSeparatedByString:@":"] count] - 1u;
    if (numParams == 0) {
        Toggle *value = [self getToggleForProperty:NSStringFromSelector([invocation selector])];
        self.lastChildToggle = value;
        [invocation setReturnValue:&value];
    }
}

- (BOOL)isEqual:(id)object {
    if (self == object) {
        return YES;
    }
    
    if (![object isKindOfClass:[Toggle class]]) {
        return NO;
    }
    
    return [self.name isEqual:((Toggle *)object).name];
}

- (NSUInteger)hash {
    return [self.name hash];
}

- (id)copyWithZone:(NSZone *)zone {
    Toggle *objectCopy = [[Toggle allocWithZone:zone] init];
    objectCopy.name = self.name;
    return objectCopy;
}

@end
