//
//  FeatureList.h
//  FeatureToggle
//
//  Created by Reinier van Vliet on 30/01/15.
//  Copyright (c) 2015 POC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "toggle.h"

@interface FeatureList : NSObject

+ (FeatureList *)sharedInstance;

-(BOOL)isEnabled:(Toggle *)featureToggle;

@end
