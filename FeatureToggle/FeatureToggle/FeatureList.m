//
//  FeatureList.m
//  FeatureToggle
//
//  Created by Reinier van Vliet on 30/01/15.
//  Copyright (c) 2015 POC. All rights reserved.
//

#import "FeatureList.h"

@interface FeatureList()

@property (nonatomic, strong) NSDictionary *features;

@end


@implementation FeatureList

- (instancetype)init
{
    self = [super init];
    if (self) {
        _features = @{
                      app : @(YES),
                      app.section1 : @(YES),
                      app.section1.subsection1 : @(YES),
                      app.section1.subsection1.feature1 : @(YES),
                      app.section1.subsection1.feature2 : @(NO),
                      };
    }
    return self;
}

+ (FeatureList *)sharedInstance{
    static FeatureList *sharedFeatureListInstance;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        sharedFeatureListInstance = [[self alloc] init];
    });
    return sharedFeatureListInstance;
}

-(BOOL)isEnabled:(Toggle *)featureToggle{
    NSString *toggleName = featureToggle.name;
    NSArray *featureGroups = [toggleName componentsSeparatedByString:@"."];
    NSAssert(featureGroups.count!=0, @"A Feature toggle group can't be empty");
    if(featureGroups.count==0){
        return NO;
    }

    NSString *featurePath = @"";
    BOOL featureEnabled = YES;
    for (NSString *featurePathPart in featureGroups){
        if(featurePath.length>0){
            featurePath = [featurePath stringByAppendingString:@"."];
        }
        featurePath = [featurePath stringByAppendingString:featurePathPart];
        NSNumber *status = _features[[[Toggle alloc] initWithName:featurePath]];
        NSAssert(status, @"A Feature toggle has been used which doesn't exist: %@", toggleName);
        featureEnabled &= [status boolValue];
    }
    
    return featureEnabled;
}

@end
