//
//  FeatureToggleTests.h
//  FeatureToggle
//
//  Created by Reinier van Vliet on 30/01/15.
//  Copyright (c) 2015 POC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "FeatureList.h"

//Some methods just for testing
@interface Toggle(Testing)
- (Toggle *)testSection1;
- (Toggle *)testSection2;
- (Toggle *)testSubsection1;
- (Toggle *)testSubsection2;
- (Toggle *)testFeature1;
- (Toggle *)testFeature2;
- (Toggle *)testFeature3;
- (Toggle *)testFeature4;
- (Toggle *)testFeature5;
- (Toggle *)testFeature6;
- (Toggle *)doesntExist;
@end


@interface FeatureList(Testing)
@property (nonatomic, strong) NSDictionary *features;
@end


@interface FeatureToggleTests : XCTestCase
@end


@implementation FeatureToggleTests

- (void)setUp {
    [super setUp];
    
    //Just for testing, override the features with some testdata
    [FeatureList sharedInstance].features =
      @{
        app : @(YES),
        app.testSection1 : @(YES),
        app.testSection1.testSubsection1 : @(YES),
        app.testSection1.testSubsection1.testFeature1 : @(YES),
        app.testSection1.testSubsection1.testFeature2 : @(NO),
        app.testSection1.testSubsection2 : @(NO),
        app.testSection1.testSubsection2.testFeature3 : @(YES),
        app.testSection1.testSubsection2.testFeature4 : @(NO),
        app.testSection2 : @(YES),
        app.testSection2.testSubsection1 : @(YES),
        app.testSection2.testSubsection1.testFeature5 : @(YES),
        app.testSection2.testSubsection2 : @(YES),
        app.testSection2.testSubsection2.testFeature6 : @(YES),
        };
}

- (void)tearDown {
    [super tearDown];
}

- (void)testBasicSwitch_ShouldBeOn {
    XCTAssertTrue(app.isEnabled);
}

- (void)testBasicSwitch_InsideParentSwitch_ShouldBeOn {
    XCTAssertTrue(app.testSection1.isEnabled);
}

- (void)testThreeLevelDeepSwitch_ShouldBeOn {
    XCTAssertTrue(app.testSection1.testSubsection1.isEnabled);
}

- (void)testDeepNestedSwitch_WhichIsOn_ShouldBeOn {
    XCTAssertTrue(app.testSection1.testSubsection1.testFeature1.isEnabled);
}

- (void)testDeepNestedSwitch_WhichIsOff_ShouldBeOff {
    XCTAssertFalse(app.testSection1.testSubsection1.testFeature2.isEnabled);
}

- (void)testDeepNestedSwitch_WhichIsOnButParentIsOff_ShouldBeOff {
    XCTAssertFalse(app.testSection1.testSubsection2.testFeature3.isEnabled);
}

- (void)testDeepNestedSwitch_WhichIsOffAndParentIsOff_ShouldBeOff {
    XCTAssertFalse(app.testSection1.testSubsection2.testFeature4.isEnabled);
}

- (void)testOtherDeepNestedSwitchWithSimilarNamedParentToggle_WhichIsOn_ShouldBeOn {
    XCTAssertTrue(app.testSection2.testSubsection1.testFeature5.isEnabled);
}

- (void)testOtherDeepNestedSwitchWithSimilarNamedParentToggle_WhichIsOff_ShouldBeOn {
    XCTAssertTrue(app.testSection2.testSubsection2.testFeature6.isEnabled);
}

- (void)testToggle_InWrongOrder_ShouldAssert{
    XCTAssertThrows(app.testFeature1.isEnabled);
}

- (void)testToggle_WhichDoesntExist_ShouldAssert{
    XCTAssertThrows(app.doesntExist.isEnabled);
}

@end
